<?php

foreach($argv as $a) {
if($a=="setup.php") continue;
mkdir("/var/www/log/$a",0755);

mkdir("/var/www/html/$a",0755);
mkdir("/var/www/html/$a/1",0755);
mkdir("/var/www/html/$a/2",0755);
touch("/var/www/html/$a/favicon.ico");

@unlink("/var/www/html/$a/libraries");
@unlink("/var/www/html/$a/logging");

symlink("/var/www/html/libraries/", "/var/www/html/$a/libraries");
symlink("/var/www/html/logging/", "/var/www/html/$a/logging");

$htaccessConf = "\n<Directory \"/var/www/html/$a\">\n   Options FollowSymLinks\n   AllowOverride All\n   Order allow,deny\n   Allow from all\n</Directory>";

$conf="<VirtualHost *:80>\n        ServerName $a        \nDocumentRoot /var/www/html/$a        \nServerAdmin webmaster@localhost        \nErrorLog /var/www/log/$a/error.log        \nCustomLog /var/www/log/$a/access.log combined\n $htaccessConf \n <IfModule php7_module>\n    php_value newrelic.appname \"$a\" \n</IfModule>\n </VirtualHost>";
file_put_contents("$a.conf",$conf);
echo `a2ensite {$a}.conf`;

$b=str_replace(".","_",$a);
echo `openssl req -nodes -newkey rsa:2048 -keyout /etc/ssl/private/{$b}.key -out /var/www/html/{$a}/{$a}.csr -subj "/C=GB/ST=London/L=London/O=Global Security/OU=IT Department/CN={$a}"`;
echo `cat /var/www/html/{$a}/{$a}.csr`;
$conf="<VirtualHost *:443> \nServerName $a \nDocumentRoot /var/www/html/$a \nServerAdmin webmaster@localhost        \nErrorLog /var/www/log/$a";
$conf.="/error.log        \nCustomLog /var/www/log/$a/access.log combined \n  <IfModule php7_module> \n    php_value newrelic.appname \"$a";
$conf.=" SSL\"  \n</IfModule>\nSSLEngine on \n SSLCertificateFile /etc/ssl/".str_replace(".","_",$a).".crt \n SSLCertificateKeyFile /etc/ssl/private/".str_replace(".","_",$a).".key \n SSLCertificateChainFile /etc/ssl/".str_replace(".","_",$a).".ca-bundle \n </VirtualHost>";
file_put_contents("$a-ssl.conf",$conf);

}
echo `systemctl reload apache2`;


